module.exports = `
<!DOCTYPE html>
<html class="bx-core bx-no-touch bx-no-retina bx-chrome">
<head>
  <meta name="viewport" content="width=1135">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="apple-itunes-app" content="app-id=561683423">
  <link rel="apple-touch-icon-precomposed" href="/images/iphone/57x57.png">
  <link rel="apple-touch-icon-precomposed" sizes="72x72" href="/images/iphone/72x72.png">
  <link rel="apple-touch-icon-precomposed" sizes="114x114" href="/images/iphone/114x114.png">
  <link rel="apple-touch-icon-precomposed" sizes="144x144" href="/images/iphone/144x144.png">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <link href="/bitrix/js/main/core/css/core.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/intranet/intranet-common.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/ui/fonts/opensans/ui.font.opensans.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/main/sidepanel/css/sidepanel.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/intranet/sidepanel/bindings/mask.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/intranet/sidepanel/bitrix24/slider.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/main/popup/dist/main.popup.bundle.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/socialnetwork/common/socialnetwork.common.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/ui/notification/ui.notification.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/main/colorpicker/css/colorpicker.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/intranet/theme_picker/theme_picker.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/tasks/css/media.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/main/loader/dist/loader.bundle.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/main/core/css/core_viewer.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/tasks/css/tasks.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/main/core/css/core_date.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/intranet/core_planner.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/ui/tooltip/tooltip.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/main/core/css/core_tooltip.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/calendar/core_planner_handler.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/timeman/timemanager/css/core_timeman.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/main/core/css/core_finder.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/ui/icons/base/ui.icons.base.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/ui/icons/b24/ui.icons.b24.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/main/helper/css/helper.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/main/phonenumber/css/phonenumber.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/ui/actionpanel/css/style.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/ui/buttons/src/css/ui.buttons.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/ui/buttons/src/css/ui.buttons.ie.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/ui/buttons/icons/ui.buttons.icons.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/ui/buttons/icons/ui.buttons.icons.ie.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/ui/viewer/css/style.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/im/css/common.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/im/css/dark_im.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/rest/css/applayout.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/im/css/phone_call_view.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/ui/alerts/src/ui.alert.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/ui/switcher/ui.switcher.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/ui/hint/ui.hint.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/disk/css/disk.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/disk/css/file_dialog.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/im/component/conference/conference-create/dist/conference-create.bundle.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/im/css/window.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/im/css/im.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/im/css/call/view.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/ui/tour/ui.tour.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/ui/tutor/ui.tutor.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/socialnetwork/css/content_view.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/lists/css/lists.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/socialnetwork/css/video_recorder.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/main/core/css/core_ui_control.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/main/core/css/core_ui_date.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/socialnetwork/selector/socialnetwork.selector.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/socialnetwork/selector/callback.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/mail/selector/mail.selector.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/mail/selector/callback.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/intranet/selector/intranet.selector.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/intranet/selector/callback.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/crm/selector/callback.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/main/core/css/core_ui_select.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/main/core/css/core_canvas.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/disk/document/style.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/disk/css/legacy_uf_common.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/css/main/font-awesome.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/fileman/html_editor/html-editor.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/ui/icons/disk/ui.icons.disk.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/ui/graph/circle/ui.circle.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/js/ui/info-helper/css/info-helper.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/components/bitrix/socialnetwork.log.ex/templates/.default/style.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/panel/main/popup.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/components/bitrix/socialnetwork.blog.post.edit/templates/.default/style.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/components/bitrix/socialnetwork.blog.blog/templates/.default/style.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/components/bitrix/socialnetwork.log.filter/templates/.default/style.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/components/bitrix/main.ui.filter/templates/.default/style.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/components/bitrix/main.ui.filter/templates/.default/themes/rounded/style.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/components/bitrix/intranet.ustat.online/templates/.default/style.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/components/bitrix/intranet.ustat/style.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/components/bitrix/tasks.widget.rolesfilter/templates/.default/style.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/components/bitrix/intranet.bitrix24.banner/templates/.default/style.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/components/bitrix/main.post.list/templates/.default/style.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/components/bitrix/rating.vote/templates/like_react/popup.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/components/bitrix/rating.vote/templates/like_react/style.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/components/bitrix/main.post.form/templates/.default/style.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/components/bitrix/main.urlpreview/templates/.default/style.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/components/bitrix/rating.vote/templates/like/popup.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/components/bitrix/socialnetwork.contentview.count/templates/.default/style.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/components/bitrix/ui.tile.selector/templates/.default/style.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/components/bitrix/socialnetwork.blog.post.preview/templates/.default/style.min.css" type="text/css" rel="stylesheet">
  <link href="/bitrix/templates/bitrix24/css/sidebar.min.css" type="text/css" rel="stylesheet">
  <link type="text/css" rel="stylesheet" href="/bitrix/js/ui/forms/ui.forms.min.css">
  <link type="text/css" rel="stylesheet" href="/bitrix/js/main/core/css/core_uf.min.css">
  <link type="text/css" rel="stylesheet" href="/bitrix/js/main/core/css/core_access.min.css">
  <link type="text/css" rel="stylesheet" href="/bitrix/js/calendar/planner.min.css">
  <link type="text/css" rel="stylesheet" href="/bitrix/components/bitrix/voting.uf/templates/.default/style.min.css">
  <link type="text/css" rel="stylesheet" href="/bitrix/components/bitrix/voting.vote.edit/templates/.default/style.min.css">
  <link type="text/css" rel="stylesheet" href="/bitrix/components/bitrix/calendar.livefeed.edit/templates/.default/style.min.css">
  <link type="text/css" rel="stylesheet" href="/bitrix/components/bitrix/lists.live.feed/templates/.default/style.min.css">
  <link href="/bitrix/components/bitrix/tasks.iframe.popup/templates/.default/style.min.css" type="text/css" data-template-style="true" rel="stylesheet">
  <link href="/bitrix/themes/.default/clock.min.css" type="text/css" data-template-style="true" rel="stylesheet">
  <link href="/bitrix/components/bitrix/intranet.search.title/templates/.default/style.min.css" type="text/css" data-template-style="true" rel="stylesheet">
  <link href="/bitrix/templates/bitrix24/components/bitrix/menu/left_vertical/groups.min.css" type="text/css" data-template-style="true" rel="stylesheet">
  <link href="/bitrix/templates/bitrix24/components/bitrix/menu/left_vertical/map.min.css" type="text/css" data-template-style="true" rel="stylesheet">
  <link href="/bitrix/templates/bitrix24/components/bitrix/menu/left_vertical/style.min.css" type="text/css" data-template-style="true" rel="stylesheet">
  <link href="/bitrix/components/bitrix/crm.card.show/templates/.default/style.min.css" type="text/css" data-template-style="true" rel="stylesheet">
  <link href="/bitrix/templates/bitrix24/components/bitrix/im.messenger/.default/style.min.css" type="text/css" data-template-style="true" rel="stylesheet">
  <link href="/bitrix/components/bitrix/bitrix24.notify.panel/templates/.default/style.min.css" type="text/css" data-template-style="true" rel="stylesheet">
  <link href="/bitrix/templates/bitrix24/template_styles.min.css" type="text/css" data-template-style="true" rel="stylesheet">
  <link href="/bitrix/templates/bitrix24/interface.min.css" type="text/css" data-template-style="true" rel="stylesheet">
  <link href="/bitrix/templates/bitrix24/themes/default/main.css" type="text/css" media="screen" data-template-style="true" data-theme-id="default" rel="stylesheet">
  <link href="/bitrix/templates/bitrix24/themes/default/menu.css" type="text/css" media="screen" data-template-style="true" data-theme-id="default" rel="stylesheet">
  <link rel="stylesheet" type="text/css" media="print" href="/bitrix/templates/bitrix24/print.css">
  <title>(1) ИНТЕРВОЛГА</title>
  <script type="text/javascript" src="/bitrix/js/main/jquery/jquery-1.8.3.min.js"></script>
</head>
<body class="template-bitrix24 no-paddings start-page bitrix24-default-theme im-bar-mode" style="">
<table class="bx-layout-table">
  <tbody>
  <tr>
    <td class="bx-layout-header">
      <div id="header" style="">
        <div id="header-inner">
          <div class="header-search"></div>
        </div>
      </div>
    </td>
  </tr>
  <tr>
    <td class="bx-layout-cont">
      <table class="bx-layout-inner-table">
        <tbody>
        <tr class="bx-layout-inner-top-row">
          <td class="bx-layout-inner-left" id="layout-left-column">
            <div class="menu-items-block" id="menu-items-block">
              <div class="menu-items-body"></div>
            </div>
          </td>
          <td class="bx-layout-inner-center" id="content-table">
            <table class="bx-layout-inner-inner-table workarea-transparent no-all-paddings no-paddings pagetitle-toolbar-field-view no-all-paddings no-all-paddings no-all-paddings no-all-paddings no-all-paddings no-all-paddings no-all-paddings no-all-paddings no-all-paddings no-all-paddings no-all-paddings no-all-paddings no-all-paddings no-all-paddings no-all-paddings no-all-paddings no-all-paddings no-all-paddings no-all-paddings">
              <tbody>
              <tr>
                <td class="bx-layout-inner-inner-cont">
                  <div id="workarea">
                    <div id="sidebar"></div>
                    <div id="workarea-content"></div>
                  </div>
                </td>
              </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td class="bx-layout-inner-left" id="layout-left-column-bottom"></td>
          <td class="bx-layout-inner-center">
            <div id="footer"></div>
          </td>
        </tr>
        </tbody>
      </table>
    </td>
  </tr>
  </tbody>
</table>
</body>
</html>
`;
