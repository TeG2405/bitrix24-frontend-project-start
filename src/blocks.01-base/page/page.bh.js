const layout = require('./layouts/base');

module.exports = function(bh) {
  bh.match('page', function(ctx, json) {
    const content = ctx.tag('body').bem(false).content();
    return layout.replace(
      /<div id="workarea-content">.*?<\/div>/,
      `<div id="workarea-content">${bh.apply(content)}</div>`)
    .replace(
      /<div id="sidebar">.*?<\/div>/,
      `<div id="sidebar">${bh.apply(json.sidebar)}</div>`);
  });
};
