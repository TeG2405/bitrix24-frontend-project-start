const fs = require('fs');
const path = require('path');
const https = require('https');
// cosnt shell = require('shelljs');

// Вернуть пути ко всем файлам в дирректории
const getAllFilesInPathSync = (src, files = [], recursive = true) => {
  fs.readdirSync(src).forEach((file) => {
    let name = path.join(src, file);
    if (fs.statSync(name).isDirectory()) {
      if (recursive) {
        getAllFilesInPathSync(name, files);
      }
    } else {
      files.push(name);
    }
  });
  return files;
};

// Вернуть пути ко всем файлам в дирректории
const getAllDirectoriesInPathSync = (src, files = [], recursive = true) => {
  fs.readdirSync(src).forEach((file) => {
    let name = path.join(src, file);
    if (fs.statSync(name).isDirectory()) {
      if (recursive) {
        files.push(name);
        getAllDirectoriesInPathSync(name, files);
      }
    }
  });
  return files;
};

const downloadAndSave = (url, dest, path, cb) => {
  if (fs.existsSync(path)) return;
  if (!fs.existsSync(dest)) fs.mkdirSync(dest, {recursive: true});
  const file = fs.createWriteStream(path);
  https.get(url, function(response) {
    response.pipe(file);
    file.on('finish', function() {
      file.close(cb);
    });
  }).on('error', function(err) {
    fs.unlink(path, (err) => console.log(path, err));
    if (cb) cb(err.message);
  });
};

module.exports = {
  getAllFilesInPathSync,
  getAllDirectoriesInPathSync,
  downloadAndSave,
};
